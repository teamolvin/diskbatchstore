const DEFAULT_FLUSH_THRESHOLD = 10;

const privates = new WeakMap();

const fs = require('fs');
const readline = require('readline');
const util = require('util');

const SemaphoreMod = require('semaphore-async-await');
const tmp = require('tmp-promise');

const Semaphore = SemaphoreMod.default;

const fsA = {
  appendFile: util.promisify(fs.appendFile),
  unlink: util.promisify(fs.unlink)
};

async function emptyDiskQueue(instance, returnItems = false) {
  const privs = privates.get(instance);
  if (!privs.haveWritten) {
    return returnItems ? [] : null;
  }

  let fileQueue = [];

  await privs.lock.wait();

  if (returnItems) {
    // create a read stream, and read file line by line from start
    const s = fs.createReadStream(privs.path);
    const lineReader = readline.createInterface(s);

    fileQueue = await new Promise(resolve => {
      const retVal = [];
      lineReader
        .on('line', lineStr => {
          const o = JSON.parse(lineStr);
          retVal.push(o);
        })
        .on('close', () => {
          resolve(retVal);
        });
    });
  }
  // clean up and remove file
  await fsA.unlink(privs.path);
  privs.path = null;
  privs.diskItemLength = 0;

  privs.lock.release();
  if (!returnItems) {
    return [];
  }

  fileQueue.push(...privs.memoryQueue);
  return fileQueue;
}

async function flushMemoryQueue(instance) {
  const privs = privates.get(instance);
  await privs.lock.wait();

  if (!privs.path) {
    const tmpObj = await tmp.file({ discardDescriptor: true });
    privs.path = tmpObj.path;
  }

  const q = privs.memoryQueue;
  if (!q.length) {
    privs.lock.release();
    return;
  }

  privs.haveDiskQueue = true;

  privs.memoryQueue = [];
  const serializedItems = q.map(JSON.stringify);

  // force a new line at the end
  serializedItems.push('');

  await fsA.appendFile(privs.path, serializedItems.join('\n'));
  privs.diskItemLength += q.length;
  privs.haveWritten = true;
  privs.lock.release();
}

class DiskBatchStore {
  constructor({ flushThreshold = DEFAULT_FLUSH_THRESHOLD } = {}) {
    privates.set(this, {
      diskItemLength: 0,
      flushThreshold,
      haveWritten: false,
      lock: new Semaphore(1),
      memoryQueue: [],
      path: null
    });
  }

  async append(item) {
    const { flushThreshold, memoryQueue } = privates.get(this);
    memoryQueue.push(item);
    if (memoryQueue.length > flushThreshold) {
      await flushMemoryQueue(this);
    }
  }

  empty() {
    const privs = privates.get(this);
    if (!privs.haveWritten) {
      const retVal = privs.memoryQueue;

      privs.memoryQueue = [];
      return retVal;
    }
    return emptyDiskQueue(this, true);
  }

  get length() {
    const privs = privates.get(this);
    return privs.diskItemLength + privs.memoryQueue.length;
  }
}

module.exports = DiskBatchStore;
