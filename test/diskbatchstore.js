const { expect } = require('chai');

const DiskBatchStore = require('../');

describe('Disk Batch Store', () => {
  describe('Stores', () => {
    it('Creates a new instance', async () => {
      const batch = new DiskBatchStore();
      expect(batch).to.be.instanceof(DiskBatchStore);
    });

    it('Appends', async () => {
      const batch = new DiskBatchStore();
      const job1 = { aap: 'noot' };
      const job2 = { aap: 'mies' };

      await batch.append(job1);
      await batch.append(job2);
      expect(batch.length).to.equal(2);
    });

    it('Appends and empties', async () => {
      const batch = new DiskBatchStore();
      const job1 = { aap: 'noot' };
      const job2 = { aap: 'mies' };

      await batch.append(job1);
      await batch.append(job2);
      expect(batch.length).to.equal(2);
      const jobs = batch.empty();

      expect(jobs).to.deep.equal([job1, job2]);
      expect(batch.length).to.equal(0);
    });

    it('Appends and empties via disk', async () => {
      const batch = new DiskBatchStore({ flushThreshold: 1 });
      const job1 = { aap: 'noot' };
      const job2 = { aap: 'mies' };

      await batch.append(job1);
      await batch.append(job2);
      expect(batch.length).to.equal(2);
      const jobs = await batch.empty();

      expect(jobs).to.deep.equal([job1, job2]);
      expect(batch.length).to.equal(0);
    });

    it('Appends and empties via disk async ok', async () => {
      const GROUP_ROW_COUNT = 20;
      const batch = new DiskBatchStore({ flushThreshold: 3 });
      const jobsGroup1 = [];
      const jobsGroup2 = [];
      const jobsGroup3 = [];
      const jobsGroup4 = [];
      for (let i = 0; i < GROUP_ROW_COUNT; i += 1) {
        jobsGroup1.push({ i, b: 1 });
        jobsGroup2.push({ i, b: 2 });
        jobsGroup3.push({ i, b: 3 });
        jobsGroup4.push({ i, b: 4 });
      }
      const boundAppend = batch.append.bind(batch);

      await Promise.all([
        Promise.all(jobsGroup1.map(boundAppend)),
        Promise.all(jobsGroup2.map(boundAppend)),
        Promise.all(jobsGroup3.map(boundAppend)),
        Promise.all(jobsGroup4.map(boundAppend))
      ]);

      expect(batch.length).to.equal(GROUP_ROW_COUNT * 4);
      const j = await batch.empty();
      expect(j).to.have.lengthOf(GROUP_ROW_COUNT * 4);
    });

    it('Can store 200 batches of 200k items of a few bytes', async function hugeTest() {
      // this test is designed to crash with an out-of-memory error
      // if everything is kept in memory

      /* eslint-disable no-console */
      this.timeout(90e3);
      console.log('hold on - this can take up to a minute');

      const batches = [];
      const BATCH_COUNT = 200;
      const ITEM_COUNT = 2e5;
      let globalCounter = 0;

      // randomly picked numbers by a throw of a dice - guaranteed to be random!
      const RANDOM_BATCH = 16;
      const RANDOM_ITEM = 42;

      for (let batchIter = 0; batchIter < BATCH_COUNT; batchIter += 1) {
        if (!(batchIter % 2)) {
          process.stdout.write(` Completed ${Math.floor(batchIter / 2)}%  \r`);
        }
        const batch = new DiskBatchStore({ flushThreshold: 5000 });
        for (let itemIter = 0; itemIter < ITEM_COUNT; itemIter += 1) {
          // eslint-disable-next-line no-await-in-loop
          await batch.append({ c: globalCounter, i: itemIter });
          globalCounter += 1;
        }
        batches.push(batch);
      }

      process.stdout.write(' Almost done!  \r');

      expect(batches[RANDOM_BATCH].length).to.equal(ITEM_COUNT);
      const x = await batches[RANDOM_BATCH].empty();
      expect(x).to.have.lengthOf(ITEM_COUNT);
      expect(x[RANDOM_ITEM].i).to.equal(RANDOM_ITEM);
      /* eslint-enable no-console */
    });
  });
});
